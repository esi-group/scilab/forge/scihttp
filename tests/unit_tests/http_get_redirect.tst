libscicurl_Init();

curl = curl_easy_init();

curl_easy_setopt(curl, CURLOPT_URL, "http://httpbin.org/redirect/1");

curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);

res = curl_easy_perform(curl);

curl_easy_cleanup(curl);

assert_checkequal(res, 0);
